<?php 
if (!isset($_SESSION['usuario'])) {
      header('Location: login.php');    
      }
if(!isset($_POST['Nombre'], $_POST['Precio'])) { exit("Error dentro de los datos del producto");}
 
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
require 'config.php';
$producto = $_POST['Nombre'];
$precio = $_POST['Precio'];
$envio = 0;
$total = $precio + $envio;

$compra = new Payer();
$compra->setPaymentMethod('paypal');

$articulo = new Item();
$articulo->setName($producto)
      ->setCurrency('MXN')
      ->setQuantity(1)
      ->setPrice($precio);      
      
$listaArticulos = new ItemList();
$listaArticulos->setItems(array($articulo));
$detalles = new Details();
$detalles->setShipping($envio)
          ->setSubtotal($precio);       
          
$cantidad = new Amount();
$cantidad->setCurrency('MXN')
          ->setTotal($total)
          ->setDetails($detalles);
          
$transaccion = new Transaction();
$transaccion->setAmount($cantidad)
               ->setItemList($listaArticulos)
               ->setDescription('Pago ')
               ->setInvoiceNumber(uniqid());               

$redireccionar = new RedirectUrls();
$redireccionar->setReturnUrl(URL_SITIO . "/index.php")
              ->setCancelUrl(URL_SITIO . "/index.php");              
              
$pago = new Payment();
$pago->setIntent("sale")
     ->setPayer($compra)
     ->setRedirectUrls($redireccionar)
     ->setTransactions(array($transaccion));    
     try { $pago->create($apiContext); } 
     catch (PayPal\Exception\PayPalConnectionException $pce) {      
       echo '<pre>';print_r(json_decode($pce->getData()));exit;}

$aprobado = $pago->getApprovalLink();
header("Location: {$aprobado}");